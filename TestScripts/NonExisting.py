import pytest
import time
from pytest_bdd import scenarios, given, when, then, parsers
from selenium import webdriver
import yaml
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.options import Options


#loading up yaml
with open(r'params.yml') as file:
    # The FullLoader parameter handles the conversion from YAML
    # scalar values to Python the dictionary format
    params = yaml.load(file, Loader=yaml.FullLoader)


# Constants
weatherSite = params["website"]
searchTerm = params["nonExistingSearchTerm"]

# Scenarios
scenarios(r'../FeatureFiles/NonExistingPostCode.feature')



# Fixtures
@pytest.fixture
def browser():
    options = Options()
    options.add_argument('--headless')
    options.add_argument('--disable-gpu')
    b = webdriver.Chrome(ChromeDriverManager().install(), chrome_options=options)
    b.implicitly_wait(20)
    yield b
    b.quit()







# Given Steps
@given('the website loads properly')
def homePage(browser):
    browser.get(weatherSite)

# when Steps
@when('the user searches a valid postcode')
def search(browser):
    try:
        search_input = browser.find_element_by_name('address')
        search_input.send_keys(searchTerm)
        browser.find_element_by_class_name("submit_3").click()
        return True
    except:
        return False

    # can also use search_input.send_keys(searchTerm, Keys.ENTER)

# then Steps
@then('the system will say Postcode not found')
def validateSearchResults(browser):
    assert ("Unable to find the postcode." in browser.find_element_by_xpath("/html/body/div/div/h1").text)
    pass



