import pytest
import time
from pytest_bdd import scenarios, given, when, then, parsers
from selenium import webdriver
import yaml
from webdriver_manager.chrome import ChromeDriverManager
from datetime import datetime
from selenium.webdriver.chrome.options import Options


#loading up yaml
with open(r'params.yml') as file:
    # The FullLoader parameter handles the conversion from YAML
    # scalar values to Python the dictionary format
    params = yaml.load(file, Loader=yaml.FullLoader)


# Constants
weatherSite = params["website"]
searchTerm = params["happySearchTerm"]

# Scenarios
scenarios(r'../FeatureFiles/HappyPathTime.feature')



# Fixtures
@pytest.fixture
def browser():
    options = Options()
    options.add_argument('--headless')
    options.add_argument('--disable-gpu')
    b = webdriver.Chrome(ChromeDriverManager().install(), chrome_options=options)
    b.implicitly_wait(20)
    yield b
    b.quit()







# Given Steps
@given('the website loads properly')
def homePage(browser):
    browser.get(weatherSite)

# when Steps
@when('the user searches a valid postcode')
def search(browser):
    try:
        search_input = browser.find_element_by_name('address')
        search_input.send_keys(searchTerm)
        browser.find_element_by_class_name("submit_3").click()
        return True
    except:
        return False

    # can also use search_input.send_keys(searchTerm, Keys.ENTER)

# then Steps
@then('a table of weather information loads')
def validateSearchResults(browser):
    table = browser.find_element_by_class_name('tableHeader')
    assert ("Weather details" in str(table.text))

@then('the Time will be a valid value')
def validateSearchResults(browser):
    table = browser.find_element_by_tag_name('tbody')
    rows = table.find_elements_by_tag_name("tr")  # get all of the rows in the table
    for row in rows:
        # Get the columns (all the column 2)
        col = row.find_elements_by_tag_name("th")
        val = row.find_elements_by_tag_name("td")
        count=0
        for items in col:
            if ("Time:" in items.text):
                assert ("Time:" in items.text)
                assert (":" in val[count].text)
                #will validate it's a valid date in the correct format
                assert(datetime.strptime(val[count].text, '%d/%m/%Y %H:%M:%S'))
            count=count+1
