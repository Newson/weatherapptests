@weatherApp @amazon
Feature: Weather Checker App
  As a user,
  I want to be able to check the weather
  So I can know whether to bring a coat.

  # The "@" annotations are tags
  # One feature can have multiple scenarios
  # The lines immediately after the feature title are just comments

  Scenario: Successful Weather Check
    Given the website loads properly
    When the user searches a valid postcode
    Then a table of weather information loads
    Then the Time will be a valid value